<?php

/**
 * @file
 * Contains \Drupal\playground\Controller\PlaygroundController.
 */

namespace Drupal\playground\Controller;

use Drupal\Core\Controller\ControllerBase;

class PlaygroundController extends ControllerBase {

  /**
   * Returns an awesome testing page.
   */
  public function playground() {
    // Play with me!

    return array('#markup' => '');
  }

}
