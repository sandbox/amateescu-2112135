<?php

/**
 * @file
 * Contains \Drupal\playground\Form\PlaygroundForm.
 */

namespace Drupal\playground\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form that displays all the config variables to edit them.
 */
class PlaygroundForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'playground_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = array();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
